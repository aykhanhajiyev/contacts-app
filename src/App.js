import React from 'react';
import { Switch,Route } from 'react-router-dom';

import { ContactsContextProvider } from './context/ContactsContext';
import ContactList from './components/ContactList';
import ContactDetails from './components/ContactDetails';

function App() {
  return (
    <ContactsContextProvider>
      <Switch>
        <Route exact path={'/'} component={()=><ContactList/>}/>
        <Route path={'/details'} component={()=><ContactDetails/>}/>
      </Switch>
    </ContactsContextProvider>
  );
}

export default App;
