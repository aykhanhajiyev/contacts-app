import React, { useContext, useState, useEffect } from "react";
import {
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Container,
  Button,
  CircularProgress,
} from "@material-ui/core";
import { Link } from "react-router-dom";

import Header from "./Header";
import ContactsContext from "../context/ContactsContext";

const ContactList = () => {
  const contactContext = useContext(ContactsContext);

  const [contactsData, setContactsData] = useState([]);
  const [loader, setLoader] = useState(false);
  useEffect(() => {
    async function fetchContacts() {
      let contactsFetched = await contactContext.getContacts();
      let contacts=[];
      if (contactsFetched != null) {
        contacts = Object.values(contactsFetched);
        let keys = Object.keys(contactsFetched);

        contacts = contacts.map((contact, ind) => ({
          ...contact,
          key: keys[ind],
        }));
      }
      setContactsData(contacts);
      setLoader(true);
    }
    fetchContacts();
  }, []);

  return (
    <Container maxwidth="sm">
      <Header />
      <TableContainer>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell>
                <b>First Name</b>
              </TableCell>
              <TableCell>
                <b>Last Name</b>
              </TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {!loader ? (
              <TableRow>
                <TableCell colSpan={3} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : null}
            {contactsData !== undefined ? (
              contactsData.map((contact) => (
                <TableRow hover key={contact.key}>
                  <TableCell>{contact.firstName}</TableCell>
                  <TableCell>{contact.lastName}</TableCell>
                  <TableCell>
                    <Link
                      to={`/details/${contact.key}`}
                      style={{ textDecoration: "none" }}
                    >
                      <Button variant="contained" color="primary">
                        See more
                      </Button>
                    </Link>
                  </TableCell>
                </TableRow>
              ))
            ) : (
              null
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
};

export default ContactList;
