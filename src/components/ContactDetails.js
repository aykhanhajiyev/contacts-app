import React, { useContext, useState, useEffect } from "react";
import {
  Container,
  makeStyles,
  Card,
  Typography,
  CardContent,
  CardActions,
  Button,
  CircularProgress,
} from "@material-ui/core";
import { useLocation, Redirect } from "react-router";

import Header from "./Header";
import ContactsContext from "../context/ContactsContext";
import ContactForm from "./ContactForm";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
    marginTop: 10,
    marginBottom: 15,
  },
  pos: {
    marginBottom: 22,
  },
  fullname: {
    fontSize: 22,
  },
});

const ContactDetails = () => {
  const classes = useStyles();

  const location = useLocation();
  const contactid = location.pathname.split("/")[2];
  const context = useContext(ContactsContext);

  const [contact, setContact] = useState({});
  const [redirect, setRedirect] = useState(false);
  useEffect(() => {
    async function getDetails() {
      const fetchedContact = await context.getContactDetails(contactid);
      setContact(fetchedContact);
    }
    getDetails();
  }, []);

  const deleteHandler = async()=>{
    const result = window.confirm("Are you sure delete it?");
    if(result){
      await context.deleteContact(contactid);
      setRedirect(true);
    }
  }
  return (
    <Container maxwidth="sm">
      <Header />
      {redirect ? <Redirect to={'/'}/> : null}
      {contact.firstName ? (
        <Card className={classes.root}>
          <CardContent>
            <Typography
              variant="h5"
              component="h2"
              className={classes.fullname}
            >
              {contact.firstName + " " + contact.lastName}
            </Typography>

            <Typography variant="h5" component="h2">
              {contact.phoneNumber}
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
              {contact.phoneNumberType}
            </Typography>

            <Typography variant="body2" component="p">
              Created date:{" "}
              {contact.createdAt.split("T")[0] +
                " " +
                contact.createdAt.split("T")[1].split(".")[0]}
            </Typography>
            <Typography variant="body2" component="p">
              Modified date:{" "}
              {contact.modifiedAt.split("T")[0] +
                " " +
                contact.modifiedAt.split("T")[1].split(".")[0]}
            </Typography>
          </CardContent>
          <CardActions>

            <ContactForm isEditMode={true} contact={contact} contactKey={contactid}/>
            
            <Button size="small" color="secondary" onClick={()=>deleteHandler()} >Delete</Button>
          </CardActions>
        </Card>
      ) : (
        <CircularProgress />
      )}
    </Container>
  );
};

export default ContactDetails;
