import React from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  makeStyles,
} from "@material-ui/core";

import AddContactForm from "./ContactForm";
import { Link } from "react-router-dom";

const Header = () => {
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    link:{
        color:'white',
        textDecoration:'none'
    }
  }));
  const classes = useStyles();
  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton
          edge="start"
          className={classes.menuButton}
          color="inherit"
          aria-label="menu"
        ></IconButton>
        <Typography variant="h6" className={classes.title}>
          <Link to={"/"} className={classes.link}>
            Contacts App
          </Link>
        </Typography>
        <AddContactForm />
      </Toolbar>
    </AppBar>
  );
};

export default Header;
