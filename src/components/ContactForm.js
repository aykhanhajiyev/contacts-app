import React, { useState, useContext } from "react";
import {
  Modal,
  makeStyles,
  Backdrop,
  Fade,
  Button,
  TextField,
  Select,
  MenuItem,
  FormControl,
} from "@material-ui/core";
import ContactsContext from "../context/ContactsContext";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width: "50%",
  },
  form: {
    "& > *": {
      marginBottom: theme.spacing(2),
    },
    display: "flex",
    flexDirection: "column",
  },
  formControl: {
    marginTop: theme.spacing(2),
    minWidth: 120,
  },
}));
const ContactForm = ({ isEditMode,contact,contactKey }) => {
  const classes = useStyles();
  const [showModal, setShowModal] = useState(false);

  const context = useContext(ContactsContext);

  const [fields, setFields] = useState({
    firstName: contact?.firstName ?? "",
    lastName: contact?.lastName ?? "",
    phoneNumberType: contact?.phoneNumberType ?? "" ,
    phoneNumber: contact?.phoneNumber ?? "",
  });

  const onChangeFieldsHandler = (event) => {
    const newInputs = {
      ...fields,
      [event.target.name]: event.target.value,
    };
    setFields(newInputs);
  };

  const addContactHandler = async() => {
    if(validationFields(fields)){
      await context.addContact(fields);
      await setShowModal(false);
      setFields({
        firstName: "",
        lastName: "",
        phoneNumberType: "",
        phoneNumber: "",
      });
      window.location.reload();
    }
    
  };

  const updateContactHandler = async() => {
    if(validationFields(fields)){
      await context.updateContact(contactKey,{...fields, createdAt:contact.createdAt});
      await setShowModal(false);
      window.location.reload();
    }
    
  };
  return (
    <>
      <Button color="inherit" onClick={() => setShowModal(true)}>
        {!isEditMode ? "Add Contact" : "Edit"}
      </Button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={showModal}
        onClose={() => setShowModal(false)}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={showModal}>
          <div className={classes.paper}>
            <h2>{!isEditMode ? "Add Contact" : "Update Contact"}</h2>
            <form className={classes.form} noValidate autoComplete="false">
              <TextField
                name="firstName"
                label="First Name"
                onChange={onChangeFieldsHandler}
                value={fields.firstName}
              />
              <TextField
                name="lastName"
                label="Last Name"
                onChange={onChangeFieldsHandler}
                value={fields.lastName}
              />
              <FormControl className={classes.formControl}>
                <Select
                  labelId="phone-number-type"
                  id="phone-type"
                  name={"phoneNumberType"}
                  value={fields.phoneNumberType}
                  onChange={onChangeFieldsHandler}
                  displayEmpty
                >
                  <MenuItem value="" disabled>
                    Phone number type
                  </MenuItem>
                  <MenuItem value={"home"}>Home</MenuItem>
                  <MenuItem value={"work"}>Work</MenuItem>
                  <MenuItem value={"mobile"}>Mobile</MenuItem>
                </Select>
              </FormControl>
              <TextField
                name="phoneNumber"
                label="Phone Number"
                onChange={onChangeFieldsHandler}
                value={fields.phoneNumber}
              />
              <Button
                variant="contained"
                color="primary"
                onClick={!isEditMode ? addContactHandler : updateContactHandler}
              >
                {!isEditMode ? "Add" : "Update"}
              </Button>
            </form>
          </div>
        </Fade>
      </Modal>
    </>
  );
};

export default ContactForm;

function validationFields(fields){
  for (const field in fields) {
    if (fields[field].trim() === "") {
      alert(`Cannot be null:${field}`);
      return false;
    }
  }
  return true
}