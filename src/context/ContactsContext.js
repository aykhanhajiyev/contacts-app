import React, { createContext } from "react";

const ContactsContext = createContext();

const API_URL = "https://contacts-app-e4d22.firebaseio.com";

export const ContactsContextProvider = ({ children }) => {
  const getContacts = async () => {
    return await fetch(API_URL + "/contacts.json")
      .then((result) => result.json())
      .then((data) => data);
  };

  const addContact = async (contact) => {
    const contactExtended = {
      ...contact,
      createdAt: new Date(),
      modifiedAt: new Date(),
    };
    return await fetch(API_URL + "/contacts.json", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(contactExtended),
    });
  };

  const getContactDetails = async (id) => {
    return await fetch(API_URL + "/contacts/" + id + ".json")
      .then((result) => result.json())
      .then((data) => data);
  };

  const deleteContact = async (key) => {
    return await fetch(API_URL + "/contacts/" + key + ".json", {
      method: "DELETE",
    })
      .then((result) => result.json())
      .then((data) => data);
  };

  const updateContact = async (key, contact) => {
    contact.modifiedAt = new Date();
    return await fetch(API_URL + "/contacts/" + key + "/.json", {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(contact),
    })
      .then((result) => result.json())
      .then((data) => data);
  };
  return (
    <ContactsContext.Provider
      value={{ getContacts, getContactDetails, addContact, deleteContact,updateContact }}
    >
      {children}
    </ContactsContext.Provider>
  );
};
export default ContactsContext;
